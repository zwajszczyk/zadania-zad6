drop table if exists entries;
create table entries (
    id integer primary key autoincrement,
    title string not null,
    isbn string not null,
	author string not null,
	publisher string not null
);