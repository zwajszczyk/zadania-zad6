# -*- coding: utf-8 -*-

import os
import microblog3
import unittest
import tempfile
#from flask import session
import beaker.middleware
import bottle
from bottle import *
import requests


class BottleTestCase(unittest.TestCase):
    db_fd = ''
    session=None


    def setUp(self):
        """
        Metoda wywoływana przed uruchomieniem każdego testu.
        Ustawia aplikację do testów i tworzy testową bazę danych.
        """
        session_opts = {
            'session.type': 'file',
            'session.data_dir': './session/',
            'session.auto': True,
            'session.logged_in' : False,
            }

        bottle.TEMPLATE_PATH.insert(0,'/home/p18/views/')

        self.app = beaker.middleware.SessionMiddleware(bottle.app(), session_opts)

        #bottle.run(app=self.app)
        #session = beaker.middleware.Session

        #self.client = self.app
        # stworzenie tymczasowego pliku na bazę testową
        # i pobranie uchwytu do pliku oraz jego nazwy
        #tempfile.
        bottle.ConfigDict.DATABASE = 'microblog2test'+str(time.time())+'.db'
        # inicjalizacja bazy
        #self.
        microblog3.init_db()

    def tearDown(self):
        """
        Metoda uruchamiana po zakończeniu każdego testu.
        Czyści testową bazę danych.
        """
        # zamknięcie tymczasowego pliku z bazą testową
        #os.close(self.db_fd)
        # skasowanie tymczasowego pliku z bazą testową
        os.unlink(bottle.ConfigDict.DATABASE)

    def test_database_setup(self):
        """
        Testuje czy baza została poprawnie zainicjalizowana.
        """
        con = microblog3.connect_db()
        # Pobranie informacji o tebeli entries
        cur = con.execute('PRAGMA table_info(entries);')
        rows = cur.fetchall()
        # Tabela entries powinna mieć 3 kolumny
        self.assertEquals(len(rows), 3)

    def test_write_entry(self):
        """
        Testuje dodawanie nowego wpisu do bazy.
        """
        expected = (u"Tytuł", u"Zawartość")
        # Symulacja żądania z URL = /
        #with self.app.test_request_context('/'):
        # Wywołanie preprocesingu dekoratorów
        #self.app.preprocess_request()
        # Dodanie nowego wpisu za pomocą dedykowanej funkcji
        microblog3.write_entry(*expected)
        # Odczyt z bazy przez DB API
        con = microblog3.connect_db()
        cur = con.execute("select * from entries;")
        rows = cur.fetchall()
        # Powinien być tylko jeden wpis
        self.assertEquals(len(rows), 1)
        # Sprawdzenie poprawności odczytu
        for val in expected:
            self.assertTrue(val in rows[0])

    def test_get_all_entries_empty(self):
        """
        Testuje brak wpisów w nowej bazie.
        """
        # Symulacja żądania z URL = /
        #with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            #self.app.preprocess_request()
            # Odczyt wszystkich wpisów za pomocą dedykowanej funkcji
        entries = microblog3.get_all_entries()
            # Nie powinno być żadnych wpisów
        self.assertEquals(len(entries), 0)

    def test_get_all_entries(self):
        """
        Testuje odczyt z bazy wszystkich wpisów.
        """
        expected = (u"Tytuł", u"Zawartość")
        # Symulacja żądania z URL = /
        #with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            #self.app.preprocess_request()
            # Dodanie nowego wpisu za pomocą dedykowanej funkcji
        microblog3.write_entry(*expected)
            # Odczyt wszystkich wpisów za pomocą dedykowanej funkcji
        entries = microblog3.get_all_entries()
            # Powinien być tylko jeden wpis
        self.assertEquals(len(entries), 1)
            # Sprawdzenie poprawności odczytu
        for entry in entries:
            self.assertEquals(expected[0], entry['title'])
            self.assertEquals(expected[1], entry['text'])



#if __name__ == '__main__':
unittest.main()