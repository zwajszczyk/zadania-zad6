# -*- coding: utf-8 -*-
__author__ = 'Zbyszek'

from bottle import *
import bottle
from bottle_sqlite import *
import sqlite3
from contextlib import closing
from beaker.middleware import *
import jinja2
from beaker.session import Session
import beaker.middleware

#DATABASE = 'microblog2.db'
#SECRET_KEY = '1234567890!@#$%^&*()'

#USERNAME = 'admin'
#PASSWORD = 'TajneHaslo'

#app=Bottle()



session_opts = {
    'session.type': 'file',
    'session.data_dir': './testsession/',
    'session.auto': True,
    'session.logged_in' : False,
    }

bottle.ConfigDict.DATABASE = 'microblog2.db'
bottle.ConfigDict.SECRET_KEY = '1234567890!@#$%^&*()'
bottle.ConfigDict.USERNAME = 'admin'
bottle.ConfigDict.PASSWORD = 'TajneHaslo'
bottle.TEMPLATE_PATH.insert(0,'/home/p18/views/')

app = beaker.middleware.SessionMiddleware(bottle.app(), session_opts)


#@route('/static/:path#.+#', name='static')
#def static(filename):
   # return static_file(filename, root='static')

@route('/static/<filename>')
def server_static(filename):
    #return static_file(filename, root='static')
    return static_file(filename, root='/home/p18/static')
    #return static_file(filename, root='')


@route('/')
@route('/show_entries')
def show_entries():
    session = request.environ['beaker.session']
    entries = get_all_entries()
    return jinja2_template('show_entries.html', entries=entries, session=session)

@route('/login', method='GET')
@route('/login', method='POST')
def login():
    session = request.environ['beaker.session']
    error = None
    if request.method == 'POST':
        try:
            do_login(request.forms['username'],
                     request.forms['password'])
        except ValueError:
            error = u"Błędne dane"
        else:
            #flash(u'Zostałeś zalogowany')
            return redirect('/show_entries')
    return jinja2_template('login.html', error=error, session=session)

@route('/logout')
def logout():
    session = request.environ['beaker.session']
    session['logged_in']=False
    session.save();
    #flash(u'Zostałeś wylogowany')
    return redirect('/show_entries')

@route('/add', method=['POST'])
def add_entry():
    session = request.environ['beaker.session']
    if session['logged_in']==False:
        abort(401)
    try:
        write_entry(request.forms['title'], request.forms['text'])
        #flash(u'Nowy wpis został dodany')
    except sqlite3.Error as e:
        pass
        #flash(u'Wystąpił błąd: %s' % e.args[0])
    return redirect('/show_entries')


def get_all_entries():
    db = connect_db()
    cur = db.execute('select title, text from entries order by id desc')
    entries = [dict(title=row[0], text=row[1]) for row in cur.fetchall()]
    db.close()
    return entries

def do_login(usr, pwd):
    session = request.environ['beaker.session']
    if usr != bottle.ConfigDict.USERNAME:
        raise ValueError
    elif pwd != bottle.ConfigDict.PASSWORD:
        raise ValueError
    else:
        session['logged_in'] = True
        session.save();

def connect_db():
    return sqlite3.connect(bottle.ConfigDict.DATABASE)

def write_entry(title, text):
    db = connect_db()
    db.execute('insert into entries (title, text) values (?, ?)',
                 [title, text])
    db.commit()
    db.close()

def init_db():
    with closing(connect_db()) as db:
        #with app.open_resource('schema.sql') as f:
            #db.cursor().executescript(f.read())
        db.execute('drop table if exists entries')
        db.execute('create table entries (id integer primary key autoincrement, title string not null, text string not null)')
        db.commit()


#if __name__ == '__main__':
    #init_db();
if __name__ == '__main__':
        #bottle.run()
        init_db()
        bottle.run(app=app, debug=True, port=31018)
        #run(host='194.29.175.240', port=31018, debug=True, app=app)
        #run(host='194.29.175.240', debug=True, app=app)
        #run(host='194.29.175.240', port=80, debug=True, app=app)